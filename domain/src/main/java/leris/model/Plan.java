package leris.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Plan domain object
 *
 * Created by ka on 27/03/16.
 */
@Entity
public class Plan {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "territoryId")
    private Territory territory;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Document> documents = new ArrayList<>();

    @Column
    private Integer budget;

    @Column
    private Date systemStartDate;

    @Column
    private Date systemEndDate;

    public Long getId() {
        return id;
    }

    public Territory getTerritory() {
        return territory;
    }

    public void setTerritory(Territory territory) {
        this.territory = territory;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public Integer getBudget() {
        return budget;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public Date getSystemStartDate() {
        return systemStartDate;
    }

    public void setSystemStartDate(Date systemStartDate) {
        this.systemStartDate = systemStartDate;
    }

    public Date getSystemEndDate() {
        return systemEndDate;
    }

    public void setSystemEndDate(Date systemEndDate) {
        this.systemEndDate = systemEndDate;
    }
}
