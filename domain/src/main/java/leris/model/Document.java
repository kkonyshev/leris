package leris.model;

import javax.persistence.*;

/**
 * Document domain object
 *
 * Created by ka on 27/03/16.
 */
@Entity
public class Document {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private String asdisCode;

    @Column
    private DocumentType documentType;

    @ManyToOne
    @JoinColumn(name = "planId")
    private Plan plan;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAsdisCode() {
        return asdisCode;
    }

    public void setAsdisCode(String asdisCode) {
        this.asdisCode = asdisCode;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }
}
