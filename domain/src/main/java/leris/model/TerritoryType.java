package leris.model;

/**
 * Created by ka on 26/03/16.
 */
public enum TerritoryType {
    PROVINCE,
    COUNTY;
}
