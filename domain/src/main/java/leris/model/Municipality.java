package leris.model;

import javax.persistence.*;

/**
 * Municipality domain object
 *
 * Created by ka on 26/03/16.
 */
@Entity
public class Municipality {
    @Id
    @GeneratedValue
    public Long id;

    @Column
    public String name;

    @Column
    public Long territoryId;

    @ManyToOne
    @JoinColumn(name = "territoryId", insertable = false, updatable = false)
    public Territory territory;

    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTerritoryId(Long territoryId) {
        this.territoryId = territoryId;
    }

    public Territory getTerritory() {
        return territory;
    }
}
