package leris.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Territory domain object
 *
 * Created by ka on 26/03/16.
 */
@Entity
public class Territory {
    @Id
    @GeneratedValue
    protected Long id;

    @Column
    protected String name;

    @Column
    protected Date systemStartDate;

    @Column
    protected Date systemEndDate;

    @Column(insertable = false, updatable = false)
    protected Long parentId;

    @ManyToOne
    @JoinColumn(name = "parentId")
    protected Territory parent;

    @Column
    @Enumerated(EnumType.STRING)
    protected TerritoryType type;

    @Column
    @Enumerated(EnumType.STRING)
    protected TerritoryLevel territoryLevel;

    @Column
    protected Boolean isCenter;

    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getSystemStartDate() {
        return systemStartDate;
    }

    public void setSystemStartDate(Date systemStartDate) {
        this.systemStartDate = systemStartDate;
    }

    public Date getSystemEndDate() {
        return systemEndDate;
    }

    public void setSystemEndDate(Date systemEndDate) {
        this.systemEndDate = systemEndDate;
    }

    public Territory getParent() {
        return parent;
    }

    public void setParent(Territory parent) {
        this.parent = parent;
    }

    public TerritoryType getType() {
        return type;
    }

    public void setType(TerritoryType type) {
        this.type = type;
    }

    public TerritoryLevel getTerritoryLevel() {
        return territoryLevel;
    }

    public void setTerritoryLevel(TerritoryLevel territoryLevel) {
        this.territoryLevel = territoryLevel;
    }

    public Boolean getCenter() {
        return isCenter;
    }

    public void setCenter(Boolean center) {
        isCenter = center;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
