package leris.model;

import javax.persistence.*;

/**
 * Sate domain object
 *
 * Created by ka on 26/03/16.
 */
@Entity
public class State {
    @Id
    @GeneratedValue
    protected Long id;

    @Column
    protected String name;

    @Column
    protected Long territoryId;

    @ManyToOne
    @JoinColumn(name = "territoryId", insertable = false, updatable = false)
    protected Territory territory;

    public Territory getTerritory() {
        return territory;
    }

    public void setTerritoryId(Long territoryId) {
        this.territoryId = territoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
