package leris.repository;

import leris.model.Plan;
import leris.model.Territory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * Created by ka on 26/03/16.
 */
@Repository
public interface PlanRepository extends CrudRepository<Plan, Long> {
    List<Plan> findByTerritory(Territory territory);
}
