package leris.repository;

import leris.model.Municipality;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * Created by ka on 26/03/16.
 */
@Repository
public interface MunicipalityRepository extends CrudRepository<Municipality, Long> {
}
