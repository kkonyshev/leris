package leris.repository;

import leris.model.Document;
import leris.model.Plan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * Created by ka on 26/03/16.
 */
@Repository
public interface DocumentRepository extends CrudRepository<Document, Long> {
    List<Document> findByPlan(Plan plan);
}
