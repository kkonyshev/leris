package leris.repository;

import leris.model.Territory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * Created by ka on 26/03/16.
 */
@Repository
public interface TerritoryRepository extends CrudRepository<Territory, Long> {
    List<Territory> findByParent(Territory parent);
}
