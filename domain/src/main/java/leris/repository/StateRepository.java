package leris.repository;

import leris.model.State;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * Created by ka on 26/03/16.
 */
@Repository
public interface StateRepository extends CrudRepository<State, Long> {
}
