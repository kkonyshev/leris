package leris.service;

import leris.Services;
import leris.model.Territory;
import leris.model.TerritoryLevel;
import leris.model.TerritoryType;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 *
 * Created by ka on 26/03/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Services.class)
public class TerritoryServiceTest {

    @Autowired
    private TerritoryService territoryService;

    @After
    public void clearRepo() {
        while (territoryService.listAll().size()>0) {
            territoryService.remove(territoryService.listAll().iterator().next());
        }
    }

    @Test
    public void testSave() {
        Territory t1 = createAnkaraTerritory();

        Territory t1Saved = territoryService.save(t1);

        Assert.assertEquals(t1.getName(), t1Saved.getName());
        Assert.assertEquals(t1.getTerritoryLevel(), t1Saved.getTerritoryLevel());
        Assert.assertEquals(t1.getType(), t1Saved.getType());
    }

    @Test
    public void testSaveWithParent() {
        Territory t1 = createAnkaraTerritory();
        Territory t1Saved = territoryService.save(t1);

        Territory t2 = createOtherTerritory();

        t2.setParent(t1Saved);

        Territory t2Saved = territoryService.save(t2);
        Territory t2Found = territoryService.findById(t2Saved.getId());

        Assert.assertEquals(t2.getName(), t2Saved.getName());
        Assert.assertEquals(t2.getTerritoryLevel(), t2Saved.getTerritoryLevel());
        Assert.assertEquals(t2.getType(), t2Saved.getType());
        Assert.assertEquals(t2Saved.getParent().getId(), t1Saved.getId());
        Assert.assertEquals(t2Found.getParent().getId(), t1Saved.getId());
    }

    @Test
    public void testGetByParentId() {
        Territory t1 = createAnkaraTerritory();
        Territory t1Saved = territoryService.save(t1);

        List<Territory> childListEmpty = territoryService.findByParentId(t1Saved.getId());
        Assert.assertEquals(0, childListEmpty.size());

        Territory t2 = createOtherTerritory();
        t2.setParent(t1Saved);
        territoryService.save(t2);

        List<Territory> childList = territoryService.findByParentId(t1Saved.getId());
        Assert.assertEquals(1, childList.size());
        Assert.assertEquals(childList.iterator().next().getParent().getId(), t1Saved.getId());
    }

    @Test
    public void testGetAll() {
        territoryService.save(createAnkaraTerritory());
        territoryService.save(createOtherTerritory());

        List<Territory> territoryList = territoryService.listAll();

        Assert.assertEquals(2, territoryList.size());
    }

    public static Territory createAnkaraTerritory() {
        Territory t1 = new Territory();
        t1.setName("Ankara");
        t1.setType(TerritoryType.PROVINCE);
        t1.setTerritoryLevel(TerritoryLevel.LEVEL_400);
        return t1;
    }

    public static Territory createOtherTerritory() {
        Territory t1 = new Territory();
        t1.setName("Other");
        t1.setType(TerritoryType.COUNTY);
        t1.setTerritoryLevel(TerritoryLevel.LEVEL_220);
        return t1;
    }
}
