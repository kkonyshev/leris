package leris.service;

import leris.Services;
import leris.model.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 *
 * Created by ka on 26/03/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Services.class)
public class DocumentServiceTest {

    @Autowired
    private DocumentService documentService;

    @After
    public void clearRepo() {
        while (documentService.listAll().size()>0) {
            documentService.remove(documentService.listAll().iterator().next());
        }
    }

    @Test
    public void testSave() {
        Document doc = createDocument1();

        Document docSaved = documentService.create(doc);
        Document docFound = documentService.findById(docSaved.getId());

        Assert.assertEquals(doc.getName(), docFound.getName());
        Assert.assertEquals(doc.getAsdisCode(), docFound.getAsdisCode());
        Assert.assertEquals(doc.getDocumentType(), docFound.getDocumentType());
    }


    @Test
    public void testGetAll() {
        documentService.create(createDocument1());
        List<Document> resList = documentService.listAll();
        Assert.assertEquals(1, resList.size());
    }

    public static Document createDocument1() {
        Document doc = new Document();
        doc.setName("doc1");
        doc.setAsdisCode("28");
        doc.setDocumentType(DocumentType.FERMAN);
        return doc;
    }
}
