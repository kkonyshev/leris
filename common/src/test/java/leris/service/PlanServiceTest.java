package leris.service;

import leris.Services;
import leris.model.Document;
import leris.model.Plan;
import leris.model.Territory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 *
 * Created by ka on 26/03/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Services.class)
public class PlanServiceTest {

    @Autowired
    private PlanService planService;

    @Autowired
    private TerritoryService territoryService;

    @Autowired
    private DocumentService documentService;

    @After
    public void clearRepo() {
        while (planService.listAll().size()>0) {
            planService.remove(planService.listAll().iterator().next());
        }
    }

    @Test
    public void savePlan() {
        Territory a = TerritoryServiceTest.createAnkaraTerritory();
        a = territoryService.save(a);

        Document doc = DocumentServiceTest.createDocument1();
        doc = documentService.create(doc);

        Plan p = new Plan();
        p.setBudget(123);
        p.setTerritory(a);
        p.getDocuments().add(doc);

        Assert.assertNull(p.getId());

        p = planService.save(p);
        Assert.assertNotNull(p.getId());
    }

    @Test
    public void findById() {
        Territory a = TerritoryServiceTest.createAnkaraTerritory();
        a = territoryService.save(a);

        Plan p = new Plan();
        p.setBudget(123);
        p.setTerritory(a);

        Plan pSaved = planService.save(p);
        Plan pFoundById = planService.findById(pSaved.getId());

        Assert.assertEquals(p.getBudget(), pFoundById.getBudget());
        Assert.assertEquals(0, pFoundById.getDocuments().size());
    }

    @Test
    public void testFindByTerritory() {
        Territory a = TerritoryServiceTest.createAnkaraTerritory();
        a = territoryService.save(a);

        Plan p = new Plan();
        p.setBudget(123);
        p.setTerritory(a);
        planService.save(p);

        List<Plan> pFoundList = planService.findByTerritory(a);
        Plan singlePlan = pFoundList.iterator().next();
        Assert.assertEquals(1, pFoundList.size());
        Assert.assertEquals(p.getBudget(), singlePlan.getBudget());
        Assert.assertEquals(0, singlePlan.getDocuments().size());
    }

    @Test
    public void testGetAllPlans() {
        Territory a = TerritoryServiceTest.createAnkaraTerritory();
        a = territoryService.save(a);

        Document doc = DocumentServiceTest.createDocument1();
        doc = documentService.create(doc);

        Plan p1 = new Plan();
        p1.setBudget(123);
        p1.setTerritory(a);
        p1.getDocuments().add(doc);
        planService.save(p1);

        Plan p2 = new Plan();
        p2.setBudget(500);
        p2.setTerritory(a);
        planService.save(p2);

        Assert.assertEquals(2, planService.listAll().size());
    }
}
