package leris.service;

import leris.model.Document;
import leris.model.Plan;
import leris.model.Territory;
import leris.repository.DocumentRepository;
import leris.repository.PlanRepository;
import leris.repository.TerritoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * Created by ka on 27/03/16.
 */
@Service
public class PlanService {

    @Autowired
    protected PlanRepository planRepository;

    @Autowired
    protected TerritoryRepository territoryRepository;

    @Autowired
    protected DocumentRepository documentRepository;

    public Plan save(Plan plan) {
        if (plan.getTerritory()==null) {
            throw new IllegalArgumentException("territory undefined");
        }
        plan.setTerritory(territoryRepository.findOne(plan.getTerritory().getId()));
        if (plan.getDocuments()!=null) {
            Iterator<Document> di = plan.getDocuments().iterator();
            while (di.hasNext()) {
                Document doc = di.next();
                doc = documentRepository.save(doc);
            }
        }
        return planRepository.save(plan);
    }

    public List<Plan> listAll() {
        List<Plan> planList = new ArrayList<>();
        for (Plan p: planRepository.findAll()) {
            planList.add(p);
        }
        return planList;
    }

    public void remove(Plan plan) {
        //TODO
        planRepository.delete(plan);
    }

    public Plan findById(Long planId) {
        return planRepository.findOne(planId);
    }

    public List<Plan> findByTerritory(Territory territory) {
        return planRepository.findByTerritory(territory);
    }
}
