package leris.service;

import leris.model.Document;
import leris.model.Plan;
import leris.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by ka on 27/03/16.
 */
@Service
public class DocumentService {

    @Autowired
    protected DocumentRepository documentRepository;


    public Document create(Document document) {
        return documentRepository.save(document);
    }

    public List<Document> listAll() {
        List<Document> documentList = new ArrayList<>();
        for (Document d: documentRepository.findAll()) {
            documentList.add(d);
        }
        return documentList;
    }

    public void remove(Document document) {
        documentRepository.delete(document);
    }

    public Document findById(Long planId) {
        return documentRepository.findOne(planId);
    }

    public List<Document> findByPlan(Plan plan) {
        return documentRepository.findByPlan(plan);
    }
}
