package leris.service;

import leris.model.Territory;
import leris.repository.TerritoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Territory service impliementation
 *
 * Created by ka on 26/03/16.
 */
@Service
public class TerritoryService {

    @Autowired
    protected TerritoryRepository territoryRepository;

    public List<Territory> listAll() {
        List<Territory> territoryList = new ArrayList<>();
        for (Territory t: territoryRepository.findAll()) {
            territoryList.add(t);
        }
        return territoryList;
    }

    public Territory findById(Long territoryId) {
        return territoryRepository.findOne(territoryId);
    }

    public Territory save(Territory territory) {
        if (territory.getParent()==null && territory.getParentId()!=null) {
            territory.setParent(territoryRepository.findOne(territory.getParentId()));
        }
        return territoryRepository.save(territory);
    }

    public List<Territory> findByParentId(Long parentId) {
        Territory parent = territoryRepository.findOne(parentId);
        //TODO
        return territoryRepository.findByParent(parent);
    }

    public void remove(Territory territory) {
        for (Territory child: territoryRepository.findByParent(territory)) {
            territoryRepository.delete(child);
        }
        territoryRepository.delete(territory);
    }
}
