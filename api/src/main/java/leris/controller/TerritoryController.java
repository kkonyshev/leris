package leris.controller;

import leris.model.Territory;
import leris.service.TerritoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * Created by ka on 27/03/16.
 */
@RestController
@RequestMapping(value = "/territory")
public class TerritoryController {

    @Autowired
    private TerritoryService territoryService;

    @RequestMapping(value = "/all", method = RequestMethod.POST)
    public List<Territory> list() {
        return territoryService.listAll();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Territory save(@RequestBody Territory territory) {
        return territoryService.save(territory);
    }

    @RequestMapping(value = "/parent/{parentId}", method = RequestMethod.POST)
    public List<Territory> getByParentId(@PathVariable("parentId") Long parentId) {
        return territoryService.findByParentId(parentId);
    }
}
