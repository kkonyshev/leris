package leris.controller;

import leris.model.Plan;
import leris.model.Territory;
import leris.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * Created by ka on 27/03/16.
 */
@RestController
@RequestMapping("/soilSurvey/plan")
public class PlanController {

    @Autowired
    private PlanService planService;

    @RequestMapping(value = "/all", method = RequestMethod.POST)
    public List<Plan> list() {
        return planService.listAll();
    }

    @RequestMapping(value = "/savePlan", method = RequestMethod.POST)
    public Plan save(@RequestBody Plan plan) {
        return planService.save(plan);
    }

    @RequestMapping(value = "/{planId}", method = RequestMethod.POST)
    public Plan getByParentId(@PathVariable("planId") Long planId) {
        return planService.findById(planId);
    }
}
