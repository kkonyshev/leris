package leris.controller;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import leris.API;
import leris.model.Plan;
import leris.model.Territory;
import leris.model.TerritoryLevel;
import leris.model.TerritoryType;
import leris.service.PlanService;
import leris.service.TerritoryService;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.List;

import static com.jayway.restassured.path.json.JsonPath.from;

/**
 *
 * Created by ka on 27/03/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = API.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class PlanControllerTest {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private TerritoryService territoryService;

    @Autowired
    private PlanService planService;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @After
    public void clearRepo() {
        while (planService.listAll().size()>0) {
            planService.remove(planService.listAll().iterator().next());
        }

        while (territoryService.listAll().size()>0) {
            Territory territory = territoryService.listAll().iterator().next();
            territoryService.remove(territory);
        }
    }

    @Test
    public void testFindById() {
        Territory territory = RestAssured.given().contentType(ContentType.JSON).body("{ \"type\": \"COUNTY\", \"territoryLevel\" : \"LEVEL_400\", \"name\": \"Ankara\" }").post("/territory/save").as(Territory.class);

        Plan plan1 = RestAssured.given().contentType(ContentType.JSON).
                body("{" +
                        "      \"territory\": {" +
                        "        \"id\": " + territory.getId() +
                        "      }," +
                        "      \"documents\": [{" +
                        "        \"name\": \"test\"," +
                        "        \"asdisCode\":28," +
                        "        \"documentType\":\"FERMAN\"" +
                        "      },{" +
                        "        \"name\": \"test2\"," +
                        "        \"asdisCode\":21," +
                        "        \"documentType\":\"FERMAN\"" +
                        "      }]," +
                        "      \"budget\": \"123\"," +
                        "      \"systemStartDate\": \"1\"," +
                        "      \"systemEndDate\": \"2\"" +
                        "    }").
                post("/soilSurvey/plan/savePlan").
                as(Plan.class);

        Plan plan = RestAssured.given().contentType(ContentType.JSON).
                post("/soilSurvey/plan/" + plan1.getId()).as(Plan.class);

        Assert.assertEquals(2, plan.getDocuments().size());
        Assert.assertEquals(territory.getId(), plan.getTerritory().getId());
        Assert.assertEquals(territory.getName(), plan.getTerritory().getName());
    }

    @Test
    public void testAll() {
        Territory territory = RestAssured.given().contentType(ContentType.JSON).body("{ \"type\": \"COUNTY\", \"territoryLevel\" : \"LEVEL_400\", \"name\": \"Ankara\" }").post("/territory/save").as(Territory.class);

        Plan plan1 = RestAssured.given().contentType(ContentType.JSON).
                body("{" +
                        "      \"territory\": {" +
                        "        \"id\": " + territory.getId() +
                        "      }," +
                        "      \"documents\": [{" +
                        "        \"name\": \"test\"," +
                        "        \"asdisCode\":28," +
                        "        \"documentType\":\"FERMAN\"" +
                        "      },{" +
                        "        \"name\": \"test2\"," +
                        "        \"asdisCode\":21," +
                        "        \"documentType\":\"FERMAN\"" +
                        "      }]," +
                        "      \"budget\": \"123\"," +
                        "      \"systemStartDate\": \"1\"," +
                        "      \"systemEndDate\": \"2\"" +
                        "    }").
                post("/soilSurvey/plan/savePlan").
                as(Plan.class);

        Plan plan2 = RestAssured.given().contentType(ContentType.JSON).
                body("{" +
                        "      \"territory\": {" +
                        "        \"id\": " + territory.getId() +
                        "      }," +
                        "      \"documents\": [{" +
                        "        \"name\": \"test\"," +
                        "        \"asdisCode\":28," +
                        "        \"documentType\":\"FERMAN\"" +
                        "      },{" +
                        "        \"name\": \"test2\"," +
                        "        \"asdisCode\":21," +
                        "        \"documentType\":\"FERMAN\"" +
                        "      }]," +
                        "      \"budget\": \"444\"," +
                        "      \"systemStartDate\": \"1\"," +
                        "      \"systemEndDate\": \"2\"" +
                        "    }").
                post("/soilSurvey/plan/savePlan").
                as(Plan.class);

        List<HashMap> planList = RestAssured.given().contentType(ContentType.JSON).
                post("/soilSurvey/plan/all").as(List.class);

        Assert.assertEquals(2, planList.size());
    }

    @Test
    public void savePlan() {
        Territory t1 = RestAssured.given().contentType(ContentType.JSON).
                body("{ \"type\": \"COUNTY\", \"territoryLevel\" : \"LEVEL_400\", \"name\": \"Ankara\" }").
                post("/territory/save").as(Territory.class);

        RestAssured.given().contentType(ContentType.JSON).
                body("{" +
                        "      \"territory\": {" +
                        "        \"id\": " + t1.getId() +
                        "      }," +
                        "      \"documents\": [{" +
                        "        \"name\": \"test\"," +
                        "        \"asdisCode\":28," +
                        "        \"documentType\":\"FERMAN\"" +
                        "      },{" +
                        "        \"name\": \"test2\"," +
                        "        \"asdisCode\":21," +
                        "        \"documentType\":\"FERMAN\"" +
                        "      }]," +
                        "      \"budget\": \"123\"," +
                        "      \"systemStartDate\": \"1\"," +
                        "      \"systemEndDate\": \"2\"" +
                        "    }").
                post("/soilSurvey/plan/savePlan").
                then().
                statusCode(HttpStatus.SC_OK).
                body("territory.type", Matchers.is(TerritoryType.COUNTY.name())).
                body("territory.territoryLevel", Matchers.is(TerritoryLevel.LEVEL_400.name()));
    }
}
