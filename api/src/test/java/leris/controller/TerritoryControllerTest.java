package leris.controller;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import leris.API;
import leris.model.Territory;
import leris.model.TerritoryLevel;
import leris.model.TerritoryType;
import leris.service.TerritoryService;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.List;

import static com.jayway.restassured.path.json.JsonPath.from;
import static org.hamcrest.Matchers.hasItems;

/**
 *
 * Created by ka on 27/03/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = API.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class TerritoryControllerTest {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private TerritoryService territoryService;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @After
    public void clearRepo() {
        while (territoryService.listAll().size()>0) {
            territoryService.remove(territoryService.listAll().iterator().next());
        }
    }

    @Test
    public void saveTerritory() {
        RestAssured.given().contentType(ContentType.JSON).
                body("{ \"type\": \"COUNTY\", \"territoryLevel\" : \"LEVEL_400\", \"name\": \"Ankara\" }").
                when().
                post("/territory/save").
                then().
                statusCode(HttpStatus.SC_OK).
                body("type", Matchers.is(TerritoryType.COUNTY.name())).
                body("territoryLevel", Matchers.is(TerritoryLevel.LEVEL_400.name()));
    }

    @Test
    public void saveTerritoryWithParent() {
        Territory t1 = RestAssured.given().contentType(ContentType.JSON).body("{ \"type\": \"COUNTY\", \"territoryLevel\" : \"LEVEL_400\", \"name\": \"Ankara\" }").post("/territory/save").as(Territory.class);

        RestAssured.given().contentType(ContentType.JSON).
                body("{ \"type\": \"PROVINCE\", \"territoryLevel\" : \"LEVEL_220\", \"name\": \"Child\", \"parentId\" : " + t1.getId() + " }").
                when().
                post("/territory/save").
                then().
                statusCode(HttpStatus.SC_OK).
                body("parentId", Matchers.is(t1.getId().intValue())).
                body("type", Matchers.is(TerritoryType.PROVINCE.name())).
                body("territoryLevel", Matchers.is(TerritoryLevel.LEVEL_220.name()));
    }

    @Test
    public void testAll() {
        Territory t1 = RestAssured.given().contentType(ContentType.JSON).body("{ \"type\": \"COUNTY\", \"territoryLevel\" : \"LEVEL_400\", \"name\": \"Ankara\" }").post("/territory/save").as(Territory.class);
        Territory t2 = RestAssured.given().contentType(ContentType.JSON).body("{ \"type\": \"PROVINCE\", \"territoryLevel\" : \"LEVEL_220\", \"name\": \"Child\", \"parentId\" : " + t1.getId() + " }").post("/territory/save").as(Territory.class);

        String response = RestAssured.given().contentType(ContentType.JSON).
                post("/territory/all").asString();

        List<Territory> territoryList = from(response).getList("");

        Assert.assertEquals(2, territoryList.size());
    }

    @Test
    public void getByParent() {
        Territory territory1 = RestAssured.given().contentType(ContentType.JSON).
                body("{ \"type\": \"COUNTY\", \"territoryLevel\" : \"LEVEL_400\", \"name\": \"Ankara\" }").
                post("/territory/save").as(Territory.class);
        Territory territory2 = RestAssured.given().contentType(ContentType.JSON).
                body("{ \"type\": \"PROVINCE\", \"territoryLevel\" : \"LEVEL_220\", \"name\": \"Child\", \"parentId\" : " + territory1.getId() + " }").
                post("/territory/save").
                as(Territory.class);

        List<HashMap> territoryList = RestAssured.given().contentType(ContentType.JSON).
                post("/territory/parent/"+territory1.getId()).as(List.class);


        Assert.assertEquals(1, territoryList.size());
        Assert.assertEquals("PROVINCE", territoryList.iterator().next().get("type"));
    }
}
