# leris

### API

Only following methods implemented now:

* ```POST: {{API-URL}}/territory/save```
* ```POST: {{API-URL}}/territory/all```
* ```POST: {{API-URL}}/territory/parent/{parentId}```
* ```POST: {{API-URL}}/soilSurvey/plan/savePlan```
* ```POST: {{API-URL}}/soilSurvey/plan/all```
* ```POST: {{API-URL}}/soilSurvey/plan/{planId}```

### Building and running tests

To build application and run integration tests type:

```sh
cd parent
mvn clean install
```

### Running application

Next commands will run application on ```localhost:8080``` with in-memory h2 databse:

```sh
cd api
mvn spring-boot:run
```

### Configuration

You can change datasrouce and application ports in configuration file:
* [api/src/main/resources/application.properties] [DS]


### Author

Konstantin Konyshev ```konyshev.konstantin@gmail.com``` [(upwork.com)] [Up]

   [DS]: <https://github.com/kkonyshev/leris/tree/master/api/src/main/resources/application.properties>
   [Up]: <https://www.upwork.com/freelancers/~01e7ebce71fba3c1db>

